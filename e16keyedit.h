#ifndef E16KEYEDIT_H
#define E16KEYEDIT_H

#if __GNUC__                    /* FIXME - Use autofoo */
#define __UNUSED__ __attribute__((unused))
#else
#define __UNUSED__
#endif

#pragma GCC diagnostic push
#if USE_GTK == 2
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#endif
#include <gtk/gtk.h>
#pragma GCC diagnostic pop

#if USE_GTK == 3
#define gtk_hbox_new(homogeneous, spacing) \
    gtk_box_new(GTK_ORIENTATION_HORIZONTAL, spacing)
#define gtk_vbox_new(homogeneous, spacing) \
    gtk_box_new(GTK_ORIENTATION_VERTICAL, spacing)
#endif

/* ipc.c */
extern int      CommsInit(void (*msg_receive_func)(char *msg));
extern void     CommsSend(const char *s);

/* menus.c */
extern GtkWidget *CreateBarSubMenu(GtkWidget * menu, const char *szName);
extern GtkWidget *CreateRightAlignBarSubMenu(GtkWidget * menu,
                                             const char *szName);
extern GtkWidget *CreateMenuItem(GtkWidget * menu, GtkAccelGroup * agrp,
                                 const char *szName,
                                 const char *szAccel, const char *szTip,
                                 GCallback func, const void *data);

#endif                          /* E16KEYEDIT_H */
