#include "config.h"

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "e16keyedit.h"

GtkWidget      *
CreateBarSubMenu(GtkWidget *menu, const char *szName)
{
    GtkWidget      *menuitem;
    GtkWidget      *submenu;

    menuitem = gtk_menu_item_new_with_label(szName);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);
    gtk_widget_show(menuitem);
    submenu = gtk_menu_new();
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), submenu);

    return submenu;
}

GtkWidget      *
CreateRightAlignBarSubMenu(GtkWidget *menu, const char *szName)
{
    GtkWidget      *menuitem;
    GtkWidget      *submenu;

    menuitem = gtk_menu_item_new_with_label(szName);
#if USE_GTK == 2
    gtk_menu_item_right_justify(GTK_MENU_ITEM(menuitem));
#endif
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);
    gtk_widget_show(menuitem);
    submenu = gtk_menu_new();
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), submenu);

    return submenu;
}

GtkWidget      *
CreateMenuItem(GtkWidget *menu, GtkAccelGroup *agrp,
               const char *szName, const char *szAccel, const char *szTip,
               GCallback func, const void *data)
{
    GtkWidget      *menuitem;

    if (szName && strlen(szName))
    {
        menuitem = gtk_menu_item_new_with_label(szName);
        if (func)
            g_signal_connect(G_OBJECT(menuitem), "activate", func,
                             (void *)data);
    }
    else
    {
        menuitem = gtk_menu_item_new();
    }

    gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);
    gtk_widget_show(menuitem);

    if (szAccel && szAccel[0] == '^')
    {
        gtk_widget_add_accelerator(menuitem, "activate", agrp,
                                   szAccel[1], GDK_CONTROL_MASK,
                                   GTK_ACCEL_VISIBLE);
    }

    if (szTip && *szTip)
        gtk_widget_set_tooltip_text(menuitem, szTip);

    return menuitem;
}
